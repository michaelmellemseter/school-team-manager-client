export default class UserRepresentation {
  constructor(
    attributes,
    credentials,
    email,
    emailVerified,
    enabled,
    firstName,
    requiredActions,
    username
    //realmRoles
  ) {
    this.attributes = attributes;
    this.credentials = credentials;
    this.email = email;
    this.emailVerified = emailVerified;
    this.enabled = enabled;
    this.firstName = firstName;
    this.requiredActions = requiredActions;
    this.username = username;
    //this.realmRoles = realmRoles
  }
}
