import "./App.css";
import Users from "./components/users/Users";
import LandingPage from "./components/LandingPage";
import Home from "./components/home/Home";
import UpcomingMatches from "./components/upcomingMatch/UpcomingMatches";
import FinishedMatches from "./components/finishedMatch/FinishedMatches";
import Coaches from "./components/coach/Coaches";
import Parents from "./components/parent/Parents";
import Players from "./components/player/Player";
import Navbar from "./components/navbar/Navbar";
import Schools from "./components/school/Schools";
import Sports from "./components/sport/Sports";
import Locations from "./components/location/Locations";
import LocationTypes from "./components/locationType/LocationTypes";
import ProfileMain from "./components/profile/ProfileMain";
import TeamsMain from "./components/team/TeamsMain";
import MessagesToAdmin from "./components/messageToAdmin/MessagesToAdmin";
import SendMessageToAdmin from "./components/messageToAdmin/SendMessageToAdmin";
import Notifications from "./components/notification/Notifications";
import { ROUTES } from "./constants/routes";
import PrivateRoute from "./hoc/PrivateRoute";
import StartRoute from "./hoc/StartRoute";
import AdminRoute from "./hoc/AdminRoute";
import AdminCoachRoute from "./hoc/AdminCoachRoute";
import ParentRoute from "./hoc/ParentRoute";
import AdminCoachPlayerRoute from "./hoc/AdminCoachPlayerRoute";
import Modal from "react-modal";
import { BrowserRouter as Router, Switch } from "react-router-dom";

Modal.setAppElement("#root");
function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
          {/*StartRoutes are routes that should NOT be accessed by logged in users. They are redirected to /home */}
          <StartRoute path={ROUTES.LandingPage} exact component={LandingPage} />

          {/*Routes to only be accessed by logged in users (all)*/}
          <PrivateRoute path={ROUTES.Home} component={Home} />
          <PrivateRoute path={ROUTES.Players} component={Players} />
          <PrivateRoute path={ROUTES.UpcomingMatches} exact component={UpcomingMatches} />
          <PrivateRoute path={ROUTES.FinishedMatches} component={FinishedMatches} />
          <PrivateRoute path={ROUTES.SendMessageToAdmin} component={SendMessageToAdmin} />
          <PrivateRoute path={ROUTES.ProfileMain} component={ProfileMain} />

          {/* Parent routes */}
          <ParentRoute path={ROUTES.Notifications} component={Notifications} />

          {/* Admin routes */}
          <AdminRoute path={ROUTES.Coaches} component={Coaches} />
          <AdminRoute path={ROUTES.Schools} component={Schools} />
          <AdminRoute path={ROUTES.LocationTypes} component={LocationTypes} />
          <AdminRoute path={ROUTES.MessagesToAdmin} component={MessagesToAdmin} />

          {/* Admin-Coach routes */}
          <AdminCoachRoute path={ROUTES.Users} component={Users} />
          <AdminCoachRoute path={ROUTES.Parents} component={Parents} />
          <AdminCoachRoute path={ROUTES.Sports} component={Sports} />
          <AdminCoachRoute path={ROUTES.Locations} component={Locations} />

          {/* Admin-Coach-Player routes */}
          <AdminCoachPlayerRoute path={ROUTES.TeamsMain} component={TeamsMain} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
