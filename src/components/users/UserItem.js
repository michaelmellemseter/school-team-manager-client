function UserItem({ user }) {
  return (
    <div className="user-item">
      <ul className="user-item-list">
        <li>User id: {user.id}</li>
        <li>
          First name: {user.firstName !== null ? user.firstName : "Not entered"}
        </li>

        <li>
          Last name: {user.surName !== null ? user.surName : "Not Entered"}
        </li>
      </ul>
    </div>
  );
}

export default UserItem;
