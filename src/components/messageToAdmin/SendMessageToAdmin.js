import {useState} from "react";
import "../../css/sendMessageToAdmin.css"
import MessageToAdminService from "../../services/MessageToAdminService";
import AuthService from "../../services/AuthService";
import messageicon from "../../assets/svg/messageicon.svg";

function SendMessageToAdmin() {

    const [title, setTitle] = useState("")
    const [message, setMessage] = useState("")
    const [confirmation, setConfirmation] = useState("")

    const onChangeTitle = event => {
        setTitle(event.target.value)
    }
    const onChangeMessage = event => {
        setMessage(event.target.value)
    }

    const sendMessage = () => {
        if (title.length === 0 || title.length === null) setConfirmation("Your message needs to have a title")
        else if (message.length === 0 || message === null) setConfirmation("Your message needs to have a message")
        else {
            const newMessage =
                {
                    "title": title,
                    "message": message,
                    "user": {
                        "id": AuthService.getDbId()
                    }
                }
            MessageToAdminService.addMessage(newMessage)
            setConfirmation("Message sent")
        }
    }

    return(
        <div>
            <div className="nav">
            </div>
            <div className="title">
                <h1>Send a message to admin</h1>
                <img width="50px" className="runner" src={messageicon} />
            </div>
            <div className="field">
                <label htmlFor="title">Title: </label>
                <input id="title" name="title" onChange={onChangeTitle}/>
            </div>
            <div className="field">
                <label>Message: </label>
                <br/>
                <textarea id="message" name="message" className="message" onChange={onChangeMessage}/>
            </div>
            <div className="field">
                <button className="add-btn" onClick={sendMessage}>Send</button>
                <br/>
                <br/>
                <p>{confirmation}</p>
            </div>
        </div>
    )
}

export default SendMessageToAdmin
