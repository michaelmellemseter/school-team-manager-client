import { useState,useEffect } from "react"
import "../../css/list.css"
import Add from "@material-ui/icons/Add";
import Modal from "react-modal";
import AuthService from "../../services/AuthService"
import LocationService from "../../services/LocationService";
import LocationTypeService from "../../services/LocationTypeService";
import Loader from "../shared/loader/Loader";
import locationicon from "../../assets/svg/locationicon.svg"

function Locations () {

    const role = AuthService.getRole()
    const [locations, setLocations] = useState([])
    const [locationTypes, setLocationTypes] = useState([])

    const [locationId, setLocationId] = useState("")
    const [name, setName] = useState("")
    const [locationType, setLocationType] = useState([])

    const [updateName, setUpdateName] = useState("")
    const [updateLocationType, setUpdateLocationType] = useState([])

    const [modalShow, setModalShow] = useState(false)
    const [updateModalShow, setUpdateModalShow] = useState(false)
    const [deleteModalShow, setDeleteModalShow] = useState(false)

    const [errorMessage, setErrorMessage] = useState("")
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        getAllLocations()
        getAllLocationTypes()
        setLoading(false)
    }, []);

    const getAllLocations = () => {
        LocationService.getAllLocations()
            .then((response) => {
                setLocations(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const getAllLocationTypes = () => {
        LocationTypeService.getAllLocationTypes()
            .then((response) => {
                setLocationTypes(response.data)
            })
            .catch(error => console.error(error.message))
    }

    const deleteLocation = (id) => {
        setDeleteModalShow(false)
        LocationService.deleteLocation(id)

        const updateState = locations.filter(function(value, index, arr) {
            return value.id !== id
        })
        setLocations(updateState)
    }

    const onChangeName = event => {
        setName(event.target.value)
    }

    const onChangeLocationTypes = event => {
        let array = locationType
        if (event.target.checked) {
            array.push(event.target.value)
            setLocationType(array)
        } else {
            array = array.filter(function(value, index, arr) {
                return value !== event.target.value
            })
            setLocationType(array)
        }
    }

    const newLocationForm = () => {
        if (name.length === 0) {
            setErrorMessage("You need to fill all the fields")
        } else {
            setModalShow(false)
            let locTypes = []
            for (let locationT of locationType) {
                locTypes.push({"id": locationT})
            }
            const newLocation =
                {
                    "name": name,
                    "locations": locTypes
                }
            LocationService.addLocation(newLocation)
            setLocationType([])

            const updateState = locations
            updateState.push(newLocation)
            setLocations(updateState)
        }
    }

    const onChangeUpdateName = event => {
        setUpdateName(event.target.value)
    }

    const onChangeUpdateLocationTypes = event => {
        let array = updateLocationType
        if (event.target.checked) {
            array.push(event.target.value)
            setUpdateLocationType(array)
        } else {
            array = array.filter(function(value, index, arr) {
                return value !== event.target.value
            })
            setUpdateLocationType(array)
        }
    }

    const setUpdateDetails = (id, name) => {
        setLocationId(id)
        setUpdateName(name)
    }

    const submitUpdate = () => {
        if (updateName.length === 0) {
            setErrorMessage("You need to fill all the fields")
        } else {
            setUpdateModalShow(false)
            let locTypes = []
            for (let locationT of updateLocationType) {
                locTypes.push({"id": locationT})
            }
            const updateLocation =
                {
                    "id": locationId,
                    "name": updateName,
                    "locations": locTypes
                }
            LocationService.updateLocation(locationId, updateLocation)
            setUpdateLocationType([])

            let updateState = locations
            for (let loc in updateState) {
                if (updateState[loc].id === locationId) {
                    updateState[loc] = updateLocation
                    break
                }
            }
            setLocations(updateState)
        }
    }

    return (
        <div className="list-main">
            <Modal closeTimeoutMS={500} isOpen={modalShow} onRequestClose={() => setModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Add New Location</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <input onChange={onChangeName} type="text" placeholder="Name" className="modal-input-field"/>
                            <label className="modal-checkbox-title">Locations Types:</label>
                            <div className="modal-checkbox-field">
                                {locationTypes.map((locationType, index) => (
                                    <div>
                                        <input className="modal-checkbox" type="checkbox" onChange={onChangeLocationTypes} value={locationType.id}/>
                                        <label className="modal-checkbox-text">{locationType.name}</label>
                                    </div>
                                ))}
                            </div>
                        </div>        
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={newLocationForm}>Add Location</div>
                    </div>
                    <div className="modal-error-message">{errorMessage}</div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={updateModalShow} onRequestClose={() => setUpdateModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Update Location</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <input onChange={onChangeUpdateName} type="text" placeholder={updateName} className="modal-input-field"/>
                            <label className="modal-checkbox-title">Locations Types:</label>
                            <div className="modal-checkbox-field">
                                {locationTypes.map((locationType, index) => (
                                    <div>
                                        <input className="modal-checkbox" type="checkbox" onChange={onChangeUpdateLocationTypes} value={locationType.id}/>
                                        <label className="modal-checkbox-text">{locationType.name}</label>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={submitUpdate}>Update Location</div>
                    </div>
                    <div className="modal-error-message">{errorMessage}</div>
                </div>
            </Modal>
            <Modal closeTimeoutMS={500} isOpen={deleteModalShow} onRequestClose={() => setDeleteModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Delete Location</h1>
                    </div>
                    <div className="modal-body">
                        <div className="modal-form">
                            <p className="team-p">Are you sure you want to delete this location?</p>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={ () => deleteLocation(locationId)}>Delete Location</div>
                    </div>
                </div>
            </Modal>
            <div className="title">
                <h1>Locations</h1>
                <img width="50px" className="runner" src={locationicon} />
            </div>
            <div className="list">
                {loading ? (
                    <div>
                        <Loader />
                    </div>
                ) : (
                    <ul>
                        {(role.includes("Admin") || role.includes("Coach")) &&
                        <div className="list-add-btn" onClick={() => setModalShow(true)}>
                            <Add fontSize="large"/> <p className="add-new-text">Add New Location</p>
                        </div>
                        }
                        {locations.map((location, index) => (
                            <div className="list-element" key={index}>
                                <div className="list-name">
                                    {location.name}
                                </div>
                                {(role.includes("Admin") || role.includes("Coach")) &&
                                    <div className="delete-update">
                                        <div onClick={() => {setUpdateDetails(location.id, location.name); setUpdateModalShow(true); }} className="update">Update</div>
                                        <div onClick={ () => {setLocationId(location.id) ; setDeleteModalShow(true)}} className="delete">Delete</div>
                                    </div>
                                }
                            </div>
                        ))}
                    </ul>
                )}
            </div>
        </div>
    )
}

export default Locations
