import "./home.css";
import InfoContainer from "./InfoContainer";
import Logo from "../../assets/run_icon.svg";
import { useEffect, useState } from "react";
import CoachService from "../../services/CoachService";
import PlayerService from "../../services/PlayerService";
import ParentService from "../../services/ParentService";
import AuthService from "../../services/AuthService";
import { MATCH_TIME } from "../../constants/matchTime";
import Loader from "../shared/loader/Loader";

function OtherHome({ role }) {
  const [previousMatches, setPreviousMatches] = useState([]);
  const [upcomingMatches, setUpcomingMatches] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const roleDbId = AuthService.getRoleDbId();
    let service; // This service will be set according to the role of the logged in user
    switch (role) {
      case "Coach":
        service = CoachService;
        break;
      case "Player":
        service = PlayerService;
        break;
      case "Parent":
        service = ParentService;
        break;
      default:
        service = null;
        break;
    }

    const fetchMatches = async (service) => {
      const previousMatches = await service.getMatches(
        roleDbId,
        MATCH_TIME.Previous
      );
      const upcomingMatches = await service.getMatches(
        roleDbId,
        MATCH_TIME.Upcoming
      );
      setPreviousMatches(previousMatches.data);
      setUpcomingMatches(upcomingMatches.data);
      setLoading(false);
    };

    if (service) fetchMatches(service);
  }, [role]);

  return (
    <div className="main-box">
      <h1 className="main-header">School Team Manager</h1>
      <br />
      {loading ? (
        <div>
          <h2>Loading your homepage...</h2>
          <Loader />
        </div>
      ) : (
        <div className="flex-container">
          <InfoContainer
            title="Previous Matches"
            titleColor="red"
            matches={previousMatches}
          />
          <img src={Logo} alt="STM logo" />
          <InfoContainer
            title="Upcoming Matches"
            titleColor="green"
            matches={upcomingMatches}
          />
        </div>
      )}
    </div>
  );
}

export default OtherHome;
