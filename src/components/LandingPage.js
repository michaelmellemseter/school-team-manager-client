import "../css/landingPage.css";
import AuthService from "../services/AuthService";
import RunIcon from "../assets/run_icon.svg";

function LandingPage() {
  return (
    <div className="main-container">
      <div className="header-main">
        <h1 className="main-title">School Team Manager</h1>
      </div>
      <div className="icon-main">
        <img src={RunIcon} alt="stm-logo" />
      </div>

      <div className="main-main">
        <div onClick={AuthService.doLogin} className="home-btn">
          Login
        </div>
      </div>
    </div>
  );
}

export default LandingPage;
