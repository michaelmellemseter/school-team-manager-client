import {useEffect,useState} from "react"
import UserService from "../../services/UserService"
import TeamService from "../../services/TeamService"
import AuthService from "../../services/AuthService"
import CoachService from "../../services/CoachService"
import Modal from "react-modal"
import coachicon from "../../assets/svg/coachicon.svg"
import teamslogo from "../../assets/svg/teamslogo.svg"
import personalia from "../../assets/svg/personalia.svg"
import "../../css/parent-profile.css"
import "../../css/coachProfile.css"

function CoachProfile() {

    const userId = AuthService.getDbId();
    const [user, setUser] = useState("")
    const [coach, setCoach] = useState("")
    const [players, setPlayers] = useState([])

    const [firstName, setFirstName] = useState("");
    const [surName, setSurName] = useState(""); 
    const [email, setEmail] = useState("");
    const [mobileNumber, setMobileNumber] = useState("");
    const [date, setDate] = useState(""); 
    const [address, setAddress] = useState(""); 
    const [teams, setTeams] = useState([]);
    const [teamName, setTeamName] = useState(""); 
    
    const [updateCoachModalShow, setUpdateCoachModalShow] = useState(false);
    const [teamsModal, setTeamsModal] = useState(false); 
    const [playerModal, setPlayerModal] = useState(false) 
    
    const [teamIndex, setTeamIndex] = useState(""); 

    useEffect(()=> {
        getUserById(userId)
        getCoachByUserId(userId)
    },[])
    
    const getUserById = (id) => {
        UserService.getUserById(id)
        .then((response) => {
            const user = response.data;
            setUser(user);
            setFirstName(user.firstName)
            setSurName(user.surName)
            setEmail(user.email)
            setMobileNumber(user.mobileNumber)
            setAddress(user.address)
            setDate(user.date)


        }).catch(error=>console.error("esrror, no user"))

    }

    const getCoachByUserId = (id) => {
        UserService.getCoachByUserId(id)
        .then((response) => {
            const coach = response.data
            setCoach(coach)
            setTeams(coach.teams) 
        })
    }

    const getPlayersByTeamId = (id) => {
        TeamService.getPlayersByTeamId(id)
        .then((response) => {
            const players = response.data
            setPlayers(players) 
        })
    }


    const tempTeam = (name, index) => {
        getPlayersByTeamId(coach.teams[index].id)
        setTeamName(name)
    }
    //update coach
    const [coachAddress, setCoachAddress] = useState("")
    const [coachMobileNumber, setCoachMobileNumber] = useState("")
    const [coachEmail, setCoachEmail] = useState("")

    const onChangeCoachAddress = event => {
        setCoachAddress(event.target.value)
    }
    const onChangeCoachMobileNumber = event => {
        setCoachMobileNumber(event.target.value)
    }
    const onChangeCoachEmail = event => {
        setCoachEmail(event.target.value)
    }

    const submitUpdate = () => {
        const updateCoach = {

            "id": coach.id,
            "user": {
                "email": coachEmail === "" ? coach.user.email: coachEmail,
                "mobileNumber": coachMobileNumber === "" ? coach.user.mobileNumber : coachMobileNumber,
                "address": coachAddress === "" ? coach.user.address : coachAddress
            }
        }

        CoachService.updateCoach(coach.id, updateCoach)

    }

    return(
        <div className="parent-profile-container">
            <Modal closeTimeoutMS={500} isOpen={updateCoachModalShow} onRequestClose={() => setUpdateCoachModalShow(false)}
                   style={{
                       overlay: {
                           position: 'fixed',
                           top: 0,
                           left: 0,
                           right: 0,
                           bottom: 0,
                           backgroundColor: 'rgba(255, 255, 255, 0.50)'
                       },
                       content: {
                           position: 'absolute',
                           top: '100px',
                           left: '400px',
                           right: '400px',
                           bottom: '100px',
                           border: '1px solid #ccc',
                           background: '#242424',
                           overflow: 'auto',
                           WebkitOverflowScrolling: 'touch',
                           borderRadius: '4px',
                           outline: 'none',
                           padding: '20px'
                       }
                   }}>
                <div className="modal-container">
                    <div className="modal-header">
                        <h1 className="modal-title">Update Coach</h1>
                    </div>

                    <div className="modal-body">
                        <div className="modal-form">

                            <input onChange={onChangeCoachMobileNumber} type="text" placeholder={"Current number: "+mobileNumber} className="modal-input-field"/>
                            <input onChange={onChangeCoachAddress} type="text" placeholder={"Current address: " + address} className="modal-input-field"/>
                            <input onChange={onChangeCoachEmail} type="text" placeholder={"Current email"+ email} className="modal-input-field"/>

                        </div>
                    </div>

                    <div className="modal-footer">
                        <div className="modal-add-btn" onClick={submitUpdate} >Update</div>
                    </div>

                </div>
            </Modal>

            <div className="parent-profile-header">
                <h1> {firstName} {surName} </h1>
            </div>
            <div className="parent-profile-body">

                <div className="parent-profile-right">
                    <div className="right-top">

                        <img width="350px" src={coachicon}/>

                    </div>

                    <div className="children-container">
                        <div className="children-cont-main">
                            <div className="children-header">
                                <img width="40px" src={teamslogo} />
                                <h3> Teams </h3>
                            </div>

                            <div className="children-body">

                                {teams.map((team, index) => (

                                    <div className="coach-element-profile" key={index}> {team.name} </div>

                                ))
                                }

                            </div>
                        </div>
                    </div>
                </div>

                <div className="parent-profile-left">
                    <div className="personalia">
                        <div className="personalia-header">
                            <img src={personalia} width="40px" />
                            <h2>Personalia</h2>
                        </div>
                        <div className="personalia-content">
                            <p> {mobileNumber}</p>
                            <p> {email}</p>
                            <p> {address}</p>
                            <p> {date}</p>
                        </div>
                    </div>
                    <div className="left-footer">
                        <div className="update-cont">
                            <div onClick= {()=> setUpdateCoachModalShow(true) } className="update-parent">
                                <div >
                                    Update Info
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CoachProfile;
