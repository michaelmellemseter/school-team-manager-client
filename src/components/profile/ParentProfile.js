import "../../css/parent-profile.css"
import {useEffect,useState} from "react"
import AuthService from "../../services/AuthService"
import UserService from "../../services/UserService"
import parenticon from "../../assets/svg/parenticon.svg"
import runner from "../../assets/svg/runner.svg"
import personalia from "../../assets/svg/personalia.svg"
import ParentService from "../../services/ParentService"
import Modal from "react-modal"
import PlayerService from "../../services/PlayerService"
import { Event } from "@material-ui/icons"

function ParentProfile() {

    const userId = AuthService.getDbId();
    const [user, setUser] = useState("")
    const [parent,setParent] = useState("")
    const [children, setChildren] = useState([])
    const [detailViewShow, setDetailViewShow] = useState(false)


 

    useEffect(()=> {
    
        getUserById(userId)
        getParentByUserId(userId)
        
    },[])

    const getUserById = (id) => {

        // UserService.getUserById(userId)
        UserService.getUserById(id)
        .then((response) => {
            console.log(response.data)
            const user = response.data; 
            setUser(user); 

        }).catch(error=>console.error("esrror, no user"))

    }

    const getParentByUserId = (id) => {
        UserService.getParentByUserId(id)
        .then((response) => {
            console.log(response.data)
            const parent = response.data; 
            setParent(parent)
            getAllChildren(parent.id)
        })
    }

    const getAllChildren = (id) => {
        ParentService.getAllChildren(id)
        .then((response) => {
            console.log(response.data)
            console.log("asdasdas")
            const children = response.data
            setChildren(children)
        })
    }

    //clicked child
    const [dob, setDob] = useState(""); 
    const [number, setNumber] = useState(""); 
    const [address, setAddress] = useState(""); 
    const [photo, setPhoto] = useState(""); 
    const [medical, setMedical] = useState("");
    const [firstName, setFirstName] = useState("");
    const [surName ,setSurName] = useState("");
    const [playerId, setPlayerId] = useState("");
    

    const clickedChild = (clickedDob, clickedMobile, clickedAddress, clickedPhoto, clickedMedical, clickedFirstName, clickedSurName, clickedPlayerId) => {
        
        // if(clickedDob !== null) {
        //     let dobFormat = clickedDob.split("T")
        //     let prettyFormat = dobFormat[0].toString()
        //     let prettyString = prettyFormat.split("-")
        //     let presentableFormat = prettyString[2] + "." + prettyString[1] + "." + prettyString[0]
        //     setDob(presentableFormat)
        //     }
        setDob(clickedDob)
        setNumber(clickedMobile)
        setAddress(clickedAddress)
        setPhoto(clickedPhoto)
        setMedical(clickedMedical)
        setFirstName(clickedFirstName)
        setSurName(clickedSurName)
        setPlayerId(clickedPlayerId)
    }

    const [updatedDob, setUpdatedDob] = useState(""); 
    const [updatedNumber, setUpdatedNumber] = useState(""); 
    const [updatedAdress, setUpdatedAdress] = useState(""); 
    const [updatedEmail, setUpdatedEmail] = useState(""); 
    const [updatedPhoto, setUpdatedPhoto] = useState(""); 
    const [updatedMedical, setUpdatedMedical] = useState("");


    const onChangeMobileNumber = event => {
        setUpdatedNumber(event.target.value)
    }
    const onChangeAddress = event => {
        setUpdatedAdress(event.target.value)
    }
    const onChangeUpdateEmail = event => {
        setUpdatedEmail(event.target.value)
    }
    const onChangeprofilePicture = event => {
        setUpdatedPhoto(event.target.value)
    }
    const onChangeMedical = event => {
        setUpdatedMedical(event.target.value)
    }
    const onChangeDob = event => {
        setUpdatedDob(event.target.value)
    }

    const updateClickedChild = () => {
            console.log(playerId)
            
            let currentAddress = "";
            let currentDob = "";
            let currentMobile = "";
            let currentPic = "";
            let currentMedical =""; 


            if(updatedAdress === "") {
                currentAddress = address
            }else{
                currentAddress = updatedAdress
                setAddress(currentAddress)
            }
            if(updatedDob === "") {
                currentDob = dob
            }else{
                currentDob = updatedDob
                setDob(currentDob)
            }
            if(updatedNumber === "") {
                currentMobile = number
            }else{
                currentMobile = updatedNumber
                setNumber(currentMobile)
            }
            if(updatedMedical === "") {
                currentMedical = medical
            }else{
                currentMedical = updatedMedical
                setMedical(currentMedical)
            }
            if(updatedPhoto === "") {
                currentPic = photo
            }else{
                currentPic = updatedPhoto
                setPhoto(currentPic)
            }

            const updatedPlayer = {
                "id": playerId,
                "medicalNotes":currentMedical,
                "profilePicture":currentPic,
                "user": {
                    "mobileNumber": currentMobile,
                    "address": currentAddress,
                    "date" : currentDob
                }
            }

        PlayerService.updatePlayerPartially(playerId,updatedPlayer)
        

    }
    //update parent
    const [updateParentModalShow, setUpdateParentModalShow] = useState(false)
    const [parentAddress, setParentAddress] = useState("")
    const [parentMobileNumber, setParentMobileNumber] = useState("")
    const [parentEmail, setParentEmail] = useState("")

    const onChangeParentAddress = event => {
        setParentAddress(event.target.value)
    }
    const onChangeParentMobileNumber = event => {
        setParentMobileNumber(event.target.value)
    }
    const onChangeParentEmail = event => {
        
        setParentEmail(event.target.value)
        
        console.log("ikke gjør noe")
        
    }

    const submitUpdate = () => {
        

        const updateParent = {
            
            "id": parent.id,
            "user": {
                "email": parentEmail === "" ? parent.user.email : parentEmail,
                "mobileNumber": parentMobileNumber ==="" ? parent.user.mobileNumber: parentMobileNumber,
                "address": parentAddress === "" ? parent.user.address : parentAddress
            }
        }

        ParentService.updateParent(parent.id, updateParent)
       
        console.log(parent.id)
        console.log(parentAddress)
        console.log(parentMobileNumber)
        console.log(parentEmail)
         
    }

    return(
        <div className="parent-profile-container">
        <Modal closeTimeoutMS={500} isOpen={detailViewShow} onRequestClose={() => setDetailViewShow(false)}
        // isOpen={detailViewShow}
        style={{
            overlay: {
              position: 'fixed',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              backgroundColor: 'rgba(255, 255, 255, 0.10)'
            },
            content: {
              position: 'absolute',
              top: '100px',
              left: '400px',
              right: '400px',
              bottom: '100px',
              border: '0px solid #ccc',
              background: '#242424',
              overflow: 'auto',
              WebkitOverflowScrolling: 'touch',
              borderRadius: '4px',
              outline: 'none',
              padding: '0px'
            }
          }}
        
        >
            <div className="modal-child-container">
                <div className="modal-child-header">
                    <h1> {firstName} {surName} </h1>
                </div>

                <div className="modal-child-body">
                    <div className="upper-modal-child-body">
                        <div className="personalia-child">
                            <p>Date Of Birth:</p>
                            <p>Mobile Number:</p>
                            <p>Address:</p>
                            <p>Medical Notes:</p>
                        </div>

                        <div className="personalia-child-db">
                            <p>{dob}</p>
                            <p>{number}</p>
                            <p>{address}</p>
                            <p>{medical}</p>
                        </div>

                        <div className="personalia-update">
                            <input type="date" onChange={onChangeDob} className="input" placeholder="Set a new Date of birth"/>
                            <input type="text" onChange={onChangeMobileNumber} className="input" placeholder="Set a new mobile number"/>
                            <input type="text" onChange={onChangeAddress} className="input" placeholder="Set a new address"/>
                            <input type="text" onChange={onChangeMedical} className="input" placeholder="Update medical notes"/>
                        </div>
                    </div>

                    <div className="lower-modal-child-body">
                        <div className="image-header">
                            <div className="image-container-profile">
                                <img id="profile-pic" src={photo} />

                            </div>
                        </div>
                      
                        <div className="setImage">
                            <input onChange={onChangeprofilePicture} placeholder="Enter URL of new image" className="input"/>
                        </div>

                    </div>
                </div>

                <div className="modal-child-footer">
                    <div onClick={updateClickedChild} className="update-child">
                        Update
                    </div>
                   
                </div>

            </div>
        </Modal>
        <Modal closeTimeoutMS={500} isOpen={updateParentModalShow} onRequestClose={() => setUpdateParentModalShow(false)}
        
        style={{
            overlay: {
              position: 'fixed',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              backgroundColor: 'rgba(255, 255, 255, 0.50)'
            },
            content: {
              position: 'absolute',
              top: '100px',
              left: '400px',
              right: '400px',
              bottom: '100px',
              border: '1px solid #ccc',
              background: '#242424',
              overflow: 'auto',
              WebkitOverflowScrolling: 'touch',
              borderRadius: '4px',
              outline: 'none',
              padding: '20px'
            }
          }}
        
        >
            <div className="modal-container">
                <div className="modal-header">
                    <h1 className="modal-title">Update Parent</h1>
                </div>

                <div className="modal-body">
                    <div className="modal-form">
                   
                        <input onChange={onChangeParentMobileNumber} type="text" placeholder="Mobile Number" className="modal-input-field"/>
                        <input onChange={onChangeParentAddress} type="text" placeholder="Address" className="modal-input-field"/>
                        <input onChange={onChangeParentEmail} type="text" placeholder="Email" className="modal-input-field"/>
                        {/* <input onChange={onChangePassword} type="password" placeholder="Password" className="input-field"/> */}
                     
                    </div>
                </div>
                
                <div className="modal-footer">
                  <div className="modal-add-btn" onClick={submitUpdate} >Update</div>
                </div>
                
            </div>
        </Modal>

           <div className="parent-profile-header">
            <h1> {user.firstName} {user.surName} </h1>
           </div>
           <div className="parent-profile-body">

           <div className="parent-profile-right">
                <div className="right-top">

                    <img width="250px" src={parenticon}/>

                </div>   

                <div className="children-container">
                <div className="children-cont-main">
                    <div className="children-header">
                        <img width="40px" src={runner} />
                        <h3> Children </h3>
                    </div>

                    <div className="children-body">
                        
                        {children.map((child, index) => (
                            
                            <div onClick={ () => {clickedChild(child.user.date, child.user.mobileNumber, child.user.address,child.profilePicture,child.medicalNotes,child.user.firstName, child.user.surName, child.id); setDetailViewShow(true)}} className="child-element" key={index}> {child.user.firstName} </div>
                            
                        ))    
                        }
                       
                    </div>
                </div>
                </div>


            </div>
           
            <div className="parent-profile-left">
                <div className="personalia">
                    <div className="personalia-header">
                        <img src={personalia} width="40px" />
                        <h2>Personalia</h2>
                    </div>
                    <div className="personalia-content">
                        <p> {user.mobileNumber}</p>
                        <p> {user.date}</p>
                        <p> {user.email}</p>
                        <p> {user.address}</p>
                    </div>
                  
                  
                </div>
                <div className="left-footer">
                    <div className="update-cont">
                        <div className="update-parent" onClick= {()=> setUpdateParentModalShow(true) }>
                            <div>
                                Update info
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                
            </div>



           </div>
        </div>
    )

}

export default ParentProfile; 
