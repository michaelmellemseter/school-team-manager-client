import PlayerService from "../../services/PlayerService"
import teamslogo from "../../assets/svg/teamslogo.svg"
import { useEffect, useState } from "react";
import AuthService from "../../services/AuthService"
import UserService from "../../services/UserService"


function PlayerTeams() {

    const userId = AuthService.getDbId();
    const [user, setUser] = useState("")
    const [player, setPlayer] = useState("")
    const [teams, setTeams] = useState([])


    useEffect(()=> {
        getUserById(userId)
        getPlayerByUserId(userId)
    },[])


    const getUserById = (id) => {
        // UserService.getUserById(userId)
        UserService.getUserById(id)
        .then((response) => {
            const user = response.data;
            setUser(user); 

        }).catch(error=>console.error("error, no user"))

    }

    const getPlayerByUserId =  (id) => {
        UserService.getPlayerByUserId(id)
        .then((response) => {
            const player = response.data;
            setPlayer(player);
            getTeamsByPlayerId(player.id) 
        }).catch(error=>console.error("no player found"))
    }
    const getTeamsByPlayerId = (playerId)=>{
        PlayerService.getTeamsByPlayerId(playerId)
            .then((response) => {
                const teams = response.data;
                setTeams(teams)
            })
    }


    return (
        <div className="coaches-main">
            <div className="nav">
            </div>

            <div className="title">
                <h1>Your Teams</h1>
                <img width="70px" className="runner" src={teamslogo} />
            </div>
            <div className="coaches-list">
                <ul>
                    {teams.map((team,index)=> (
                        <div className="coach-element" key={index}>
                            <div className="coach-name">
                                {team.name} {team.teamPlayers[index].jerseyNumber}
                            </div>

                        </div>
                    ))}

                </ul>
            </div>
        </div>
    )
}


export default PlayerTeams; 

