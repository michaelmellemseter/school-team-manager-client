import Modal from "react-modal";
import { useState } from "react";
import styles from "./NotifyModal.module.css";
import AuthService from "../../../services/AuthService";

function NotifyModal({ showing, closeFunction, receiverId, sendFunction }) {
  const [text, setText] = useState("");
  const senderId = AuthService.getDbId();

  return (
    <Modal
      closeTimeoutMS={500}
      isOpen={showing}
      onRequestClose={() => closeFunction(false)}
      style={{
        overlay: {
          position: "fixed",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          backgroundColor: "rgba(255, 255, 255, 0.50)",
        },
        content: {
          position: "absolute",
          top: "100px",
          left: "400px",
          right: "400px",
          bottom: "100px",
          border: "1px solid #ccc",
          background: "#242424",
          overflow: "auto",
          WebkitOverflowScrolling: "touch",
          borderRadius: "4px",
          outline: "none",
          padding: "20px",
        },
      }}
    >
      <div className="modal-container">
        <div className="modal-header">
          <h1 className="modal-title">Notify Parents</h1>
        </div>
        <div className="modal-body">
          <div className="modal-form">
            <textarea
              onChange={(evt) => setText(evt.target.value)}
              type="text"
              placeholder="Write something..."
              className={styles.NotifyTextArea}
            />
          </div>
        </div>
        <div className="modal-footer">
          <div
            className="modal-add-btn"
            onClick={() => sendFunction(senderId, receiverId, text)}
          >
            Send notification
          </div>
        </div>
      </div>
    </Modal>
  );
}

export default NotifyModal;
