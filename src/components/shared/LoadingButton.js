function LoadingButton({ children, type, loading, error, message, onclick }) {
  return (
    <div>
      <button
        className="add-btn"
        type={type}
        disabled={loading}
        onClick={onclick}
      >
        {loading && <i className="fa fa-circle-o-notch fa-spin"></i>}
        {children}
      </button>
      {error && <p className="error">Something went wrong: {error}</p>}
      {message && <p className="success">{message}</p>}
    </div>
  );
}

export default LoadingButton;
