import axios from "axios";
import HttpService from "./HttpService";
import UserRepresentation from "../models/UserRepresentation";
import RoleRepresentation from "../models/RoleRepresentation";

const USERS_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/users`;

const axiosClient = HttpService.getAxiosClient();

class UserService {
  getAllUsers() {
    return axiosClient.get(USERS_REST_API_URL);
  }
  getPlayerByUserId(id) {
    return axiosClient.get(`${USERS_REST_API_URL}/${id}/player`);
  }
  getParentByUserId(id) {
    return axiosClient.get(`${USERS_REST_API_URL}/${id}/parent`);
  }
  getCoachByUserId(id) {
    return axiosClient.get(`${USERS_REST_API_URL}/${id}/coach`);
  }
  deleteUser(id) {
    axiosClient.delete(USERS_REST_API_URL + "/" + id);
  }
  async postUser(user, role) {
    // Get keycloak token so we can make requests
    const token = await this.getToken();

    // Create axios obj with bearer token for Keycloak
    const keycloakAxios = axios.create();

    keycloakAxios.interceptors.request.use((config) => {
      config.headers.Authorization = `Bearer ${token}`;
      return config;
    });

    // POST user to DB
    let dbId;
    let roleDbId;
    let oneTimePassword;
    await axiosClient
      .post(`${process.env.REACT_APP_API_BASE_URL}/users`, user)
      .then((res) => {
        dbId = res.data.user.id;
        roleDbId = res.data.roleDbId;
        oneTimePassword = res.data.password;
      })
      .catch((err) => {
        throw "Something went wrong when creating the user";
      });

    // Create the user to post to Keycloak
    const attributes = { dbId: dbId, roleDbId: roleDbId };
    const credentials = [{ value: oneTimePassword }];
    const emailVerified = true;
    const enabled = true;
    const firstName = user.firstName;
    const requiredActions = ["UPDATE_PASSWORD", "CONFIGURE_TOTP"];
    const username = user.email;
    //const realmRoles = {name: passedRole.roleName}
    const userRepresentation = new UserRepresentation(
      attributes,
      credentials,
      user.email,
      emailVerified,
      enabled,
      firstName,
      requiredActions,
      username
      //realmRoles
    );
    // POST user to Keycloak
    await keycloakAxios
      .post(
        `https://stm-cors-anywhere.herokuapp.com/${process.env.REACT_APP_KEYCLOAK_API_BASE_URL}/admin/realms/School-team-manager/users`,
        userRepresentation
      )
      .catch((err) => {
        throw "Error when posting user to Keycloak";
      });

    // GET the id of the created user from Keycloak
    let id;
    await keycloakAxios
      .get(
        `https://stm-cors-anywhere.herokuapp.com/${process.env.REACT_APP_KEYCLOAK_API_BASE_URL}/admin/realms/School-team-manager/users?email=${user.email}`,
        { email: user.email }
      )
      .then((res) => (id = res.data[0].id))
      .catch((err) => {
        throw "Could not get the id of the created user";
      });

    // POST realm-level role mapping to the user
    const keycloakId = role.keycloakId;
    const name = role.roleName;
    const composite = false;
    const clientRole = false;
    const containerId = "School-team-manager";
    const roleRepresentation = new RoleRepresentation(
      keycloakId,
      name,
      composite,
      clientRole,
      containerId
    );
    await keycloakAxios
      .post(
        `https://stm-cors-anywhere.herokuapp.com/${process.env.REACT_APP_KEYCLOAK_API_BASE_URL}/admin/realms/School-team-manager/users/${id}/role-mappings/realm`,
        [roleRepresentation]
      )
      .catch((err) => {
        throw "Error when setting the role of the user";
      });
  }

  async getToken() {
    // Params (x-www-form-urlencoded)
    const params = new URLSearchParams();
    params.append("username", process.env.REACT_APP_KEYCLOAK_USERNAME);
    params.append("password", process.env.REACT_APP_KEYCLOAK_PASSWORD);
    params.append("client_id", "admin-cli");
    params.append("grant_type", "password");

    // Set content type
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };

    const token = await axios
      .post(
        `https://stm-cors-anywhere.herokuapp.com/${process.env.REACT_APP_KEYCLOAK_API_BASE_URL}/realms/master/protocol/openid-connect/token`,
        params,
        config
      )
      .then((res) => {
        return res.data.access_token;
      })
      .catch((err) => {
        throw "Error getting the Keycloak Token";
      });

    return token;
  }
  getUserById(id) {
    return axiosClient.get(USERS_REST_API_URL + "/" + id);
  }
  getNotifications(id) {
    return axiosClient.get(`${USERS_REST_API_URL}/${id}/notifications`)
  }
}

export default new UserService();
