import HttpService from "./HttpService"

const LOCATION_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/locations`
const axios = HttpService.getAxiosClient()

class LocationService {
    getAllLocations() {
        return axios.get(LOCATION_REST_API_URL)
    }
    addLocation(location) {
        axios.post(LOCATION_REST_API_URL, location)
    }
    updateLocation(id, location) {
        axios.put(`${LOCATION_REST_API_URL}/${id}`, location)
    }
    deleteLocation(id) {
        axios.delete(`${LOCATION_REST_API_URL}/${id}`)
    }
}

export default new LocationService()
