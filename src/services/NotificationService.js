import NotificationDTO from "../models/NotificationDTO";
import HttpService from "./HttpService";

const NOTIFICATIONS_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/notifications`;
const axios = HttpService.getAxiosClient();

class NotificationService {
  setNotificationSeen(id) {
    axios.patch(`${NOTIFICATIONS_REST_API_URL}/${id}/seen`);
  }

  // Send out notification to parents in team
  postNotificationTeam(senderId, receiverId, message) {
    const notificationDTO = new NotificationDTO(senderId, receiverId, message);
    axios
      .post(`${NOTIFICATIONS_REST_API_URL}/team`, notificationDTO)
      .catch(() => {
        throw "Something went wrong sending the notification";
      });
  }

  // Send out notification to parents in school
  postNotificationSchool(senderId, receiverId, message) {
    const notificationDTO = new NotificationDTO(senderId, receiverId, message);
    axios
      .post(`${NOTIFICATIONS_REST_API_URL}/school`, notificationDTO)
      .catch(() => {
        throw "Something went wrong sending the notification";
      });
  }

  // Send out notification to parents in sport
  postNotificationSport(senderId, receiverId, message) {
    const notificationDTO = new NotificationDTO(senderId, receiverId, message);
    axios
      .post(`${NOTIFICATIONS_REST_API_URL}/sport`, notificationDTO)
      .catch(() => {
        throw "Something went wrong sending the notification";
      });
  }

  // Send out notification to parents of player
  postNotificationPlayer(senderId, receiverId, message) {
    const notificationDTO = new NotificationDTO(senderId, receiverId, message);
    axios
      .post(`${NOTIFICATIONS_REST_API_URL}/player`, notificationDTO)
      .catch(() => {
        throw "Something went wrong sending the notification";
      });
  }
}

export default new NotificationService();
