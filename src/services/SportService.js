import HttpService from "./HttpService"

const SPORT_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/sports`
const axios = HttpService.getAxiosClient()

class SportService {
    getAllSports() {
        return axios.get(SPORT_REST_API_URL)
    }
    addSport(sport) {
        axios.post(SPORT_REST_API_URL, sport)
    }
    updateSport(id, sport) {
        axios.put(`${SPORT_REST_API_URL}/${id}`, sport)
    }
    deleteSport(id) {
        axios.delete(`${SPORT_REST_API_URL}/${id}`)
    }
}

export default new SportService()
