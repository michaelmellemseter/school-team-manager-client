import HttpService from "./HttpService";

const PARENT_REST_API_URL = "https://school-team-manager.herokuapp.com/api/v1/parents";
const axios = HttpService.getAxiosClient();

class ParentService {
  getAllParents() {
    return axios.get(PARENT_REST_API_URL);
  }
  getAllChildren(parent) {
    return axios.get(PARENT_REST_API_URL +"/" + parent + "/players");
  }
  getParentById(id) {
    return axios.get(PARENT_REST_API_URL +"/" + id);
  }
  updateParent(id, parent) {
    axios.patch(PARENT_REST_API_URL+"/" + id, parent);
  }
  getMatches(id, time) {
    return axios.get(
      `${PARENT_REST_API_URL}/${id}/children-matches?time=${time}`
    );
  }
}

export default new ParentService();
