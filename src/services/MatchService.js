import HttpService from "./HttpService"

const MATCH_REST_API_URL = `${process.env.REACT_APP_API_BASE_URL}/matches`
const axios = HttpService.getAxiosClient()

class MatchService {
    getAllUpcomingMatches(teams) {
        return axios.get(`${MATCH_REST_API_URL}/upcoming`, teams)
    }
    getAllFinishedMatches(teams) {
        return axios.get(`${MATCH_REST_API_URL}/finished`, teams)
    }
    addMatch(match) {
        axios.post(MATCH_REST_API_URL, match)
    }
    setScore(id, match) {
        axios.patch(`${MATCH_REST_API_URL}/${id}/score`, match)
    }
    setReason(id, match) {
        axios.patch(`${MATCH_REST_API_URL}/${id}/cancel`, match)
    }
}

export default new MatchService()
