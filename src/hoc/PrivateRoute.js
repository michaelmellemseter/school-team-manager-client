import { Redirect, Route } from "react-router-dom";
import AuthService from "../services/AuthService";
import { ROUTES } from "../constants/routes";

export const PrivateRoute = (props) => {
  const authenticated = AuthService.isLoggedIn();

  // If user is not logged in, then redirect user to the start page
  if (!authenticated) return <Redirect to={ROUTES.LandingPage} />;

  return <Route {...props} />;
};

export default PrivateRoute;
