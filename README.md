# School Team Manager
### by Morten Svedjan Dahl, Håkon Dalen Hestnes, Michael Mellemseter and Jakkris Thongma

A page for your school to organize and have more control over it's different sports activities.  
The page is made using React and it is making use of School Team Manager API as an API.

You can access the page at https://school-team-manager-client.herokuapp.com/, but you can also clone this project and run it locally.

## How to setup
The project is using an environment file, so to run the project locally you need to add an .env-cmdrc file in the root folder.
This file needs to contain different urls and loggin to Keycloak and should look something like this:  

```javascript
{
    "dev": {
        "REACT_APP_KEYCLOAK_USERNAME": "",
        "REACT_APP_KEYCLOAK_PASSWORD": "",
        "REACT_APP_KEYCLOAK_API_BASE_URL": "https://keycloak-school-team-managerv2.herokuapp.com/auth",
        "REACT_APP_API_BASE_URL": "http://localhost:8080/api/v1",
        "REACT_APP_KEYCLOAK_REDIRECT_URL": "http://localhost:3000"
    }
}
```
(You need to insert your own username and password for Keycloak)  
This is assumes that you run the API locally aswell, but if you want to use
the deployed API you can switch the API url with: 
```javascript
"REACT_APP_API_BASE_URL": "https://school-team-manager.herokuapp.com/api/v1"
```

## How to run
To run the project locally run this in the root folder:
```bash
npm start
```

